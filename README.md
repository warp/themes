# emacs-themes

## Hackage
![screenshot](/parsley/themes/src/branch/master/hackage.png)

Port of the hackage.haskell.org default source theme, for use with haskell-mode.

### Limitations
True to the original, function names are no longer a distinct face.
Some highlighting from hackage is unsupported by haskell mode:

* cpp face - same as pragma face
* glyph face - some glyphs such as parens (see [below](#paren-face)), commas &
semicolons appear in default face

Expect some incompatibility with most packages that add additional faces.

### Compatibility
#### paren-face
Adds correct "glyph" face to parens (orange-red, similar to operators).
