(deftheme hackage
  "Port of the hackage.haskell.org default source theme, for use with haskell-mode.

True to the original, function names are no longer a distinct face.
  Some highlighting from hackage is unsupported by haskell mode:
    cpp - same as pragma face
    glyphs - some appear in default face; parens supported with paren-face")

(custom-theme-set-faces
 'hackage
 '(default ((t (:background "#fdf6e3" :foreground "#073642"))))
 '(font-lock-type-face ((t (:foreground "#5f5faf"))))
 '(font-lock-keyword-face ((t (:foreground "#af005f"))))
 '(font-lock-comment-face ((t (:foreground "#8a8a8a"))))
 '(font-lock-string-face ((t (:foreground "#cb4b16"))))
 '(font-lock-variable-name-face ((t (:foreground "#dc322f"))))
 '(font-lock-builtin-face ((t (:foreground "#859900"))))
 '(font-lock-constant-face ((t (:foreground "#149bda"))))
 '(font-lock-preprocessor-face ((t (:foreground "#2aa198"))))
 '(font-lock-doc-face ((t (:inherit font-lock-comment-face))))
 '(font-lock-function-name-face ((t nil)))
 '(region ((t (:background "#eee8d5"))))
 '(magit-section-highlight ((t (:background "#eee8d5"))))
 '(highlight ((t (:background "#eee8d5"))))
 '(parenthesis ((t (:foreground "#dc322f"))))
 '(mode-line ((t (:background "#eee8d5"))))
 '(fringe ((t (:background "#eee8d5")))))

(provide-theme 'hackage)
